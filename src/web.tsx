import { StrictMode } from 'react'
import { hydrateRoot } from 'react-dom/client'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'

import { RootState, setupStore } from './core'

import App from './web/App'
import ErrorBoundary from './web/components/errorBoundary/errorBoundary'

import './styles.css'

declare global {
  interface Window {
    __state: RootState
  }
}

const store = setupStore(window.__state)

hydrateRoot(
  document.getElementById('app') as HTMLElement,
  <StrictMode>
    <Provider store={store}>
      <ErrorBoundary>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ErrorBoundary>
    </Provider>
  </StrictMode>
)
