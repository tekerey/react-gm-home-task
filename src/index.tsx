import { StrictMode } from 'react'
import { createRoot } from 'react-dom/client'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import { setupStore } from './core'

import App from './web/App'
import ErrorBoundary from './web/components/errorBoundary/errorBoundary'

import './styles.css'

const store = setupStore()

const root = createRoot(document.getElementById('app') as HTMLElement)

root.render(
  <StrictMode>
    <Provider store={store}>
      <ErrorBoundary>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ErrorBoundary>
    </Provider>
  </StrictMode>
)
