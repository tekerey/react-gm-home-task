import { useParams, useSearchParams } from 'react-router-dom'
import { useGetMoviesQuery } from '../../core/movies'
import { Genre } from '../../types/interfaces'

export const useGetMoviesQueryWithParams = ({
  movieId,
}: {
  movieId?: string | null
} = {}) => {
  const { searchQuery: search } = useParams()
  const [query] = useSearchParams()

  const filterBy = query.get('genre') as Genre | null
  const sortBy = query.get('sortBy')

  return useGetMoviesQuery(
    { search, filterBy, sortBy },
    {
      ...(movieId && {
        selectFromResult: result => {
          return {
            ...result,
            selectedMovie: result.data?.find(({ id }) => id === +movieId),
          }
        },
      }),
    }
  )
}
