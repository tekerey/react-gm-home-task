import { useEffect, useRef, MouseEvent } from 'react'

export const useOutsideClick = <T extends HTMLElement>(
  handler: (event: MouseEvent<T>) => void
) => {
  const ref = useRef<T>(null)

  useEffect(() => {
    const handleClick = (event: any) => {
      if (ref.current && !ref.current.contains(event.target as Node)) {
        handler?.(event)
      }
    }

    document.addEventListener('pointerdown', handleClick)

    return () => {
      document.removeEventListener('pointerdown', handleClick)
    }
  }, [handler])

  return ref
}
