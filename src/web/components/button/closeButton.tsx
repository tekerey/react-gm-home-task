import { FC } from 'react'
import cn from 'classnames'

import styles from './closeButton.module.css'

export interface CloseButtonProps {
  onClick?: () => void
  className?: string
  size?: 'small' | 'medium'
  'data-test-id'?: string
}

const CloseButton: FC<CloseButtonProps> = ({
  onClick,
  className,
  size = 'medium',
  'data-test-id': dataTestId,
}) => {
  return (
    <div className={className}>
      <button
        data-test-id={dataTestId}
        className={cn(styles.CloseButton, styles[`CloseButton--${size}`])}
        onClick={onClick}
      />
    </div>
  )
}

export default CloseButton
