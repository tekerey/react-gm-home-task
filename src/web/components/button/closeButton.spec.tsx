import userEvent from '@testing-library/user-event'

import { render, screen } from '../../utils/test-utils'

import CloseButton from './closeButton'

describe('<CloseButton />', () => {
  it('should render medium CloseButton by default', () => {
    const { asFragment } = render(<CloseButton />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render small CloseButton', () => {
    const { asFragment } = render(<CloseButton size="small" />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should fire callback on click', async () => {
    const onClick = jest.fn()
    render(<CloseButton onClick={onClick} />)

    await userEvent.click(screen.getByRole('button'))

    expect(onClick).toHaveBeenCalledTimes(1)
  })
})
