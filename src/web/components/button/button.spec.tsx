import userEvent from '@testing-library/user-event'

import { render, screen } from '../../utils/test-utils'

import Button, { LinkButton } from './button'

describe('<Button />', () => {
  const buttonText = 'Click me'

  it('should render Button component', () => {
    const { asFragment } = render(<Button>{buttonText}</Button>)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render Button component with a secondary style', () => {
    const { asFragment } = render(<Button secondary>{buttonText}</Button>)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render Button component with a text', () => {
    render(<Button>{buttonText}</Button>)

    expect(screen.getByText(buttonText)).toBeInTheDocument()
  })

  it('should fire callback on click', async () => {
    const onClick = jest.fn()
    render(<Button onClick={onClick}>{buttonText}</Button>)

    await userEvent.click(screen.getByText(buttonText))

    expect(onClick).toHaveBeenCalledTimes(1)
  })
})

describe('<LinkButton />', () => {
  const buttonText = 'Click me'
  const to = '/search'

  it('should render LinkButton component', () => {
    const { asFragment } = render(<LinkButton to={to}>{buttonText}</LinkButton>)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render LinkButton component with a secondary style', () => {
    const { asFragment } = render(
      <LinkButton to={to} secondary>
        {buttonText}
      </LinkButton>
    )

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render LinkButton component with a text', () => {
    render(<LinkButton to={to}>{buttonText}</LinkButton>)

    expect(screen.getByText(buttonText)).toBeInTheDocument()
  })

  it('should have the correct href attribute', () => {
    render(<LinkButton to={to}>{buttonText}</LinkButton>)

    expect(screen.getByText(buttonText)).toHaveAttribute('href', to)
  })
})
