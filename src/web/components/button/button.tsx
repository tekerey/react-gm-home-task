import {
  AnchorHTMLAttributes,
  ButtonHTMLAttributes,
  FC,
  ReactNode,
} from 'react'
import { Link } from 'react-router-dom'
import cn from 'classnames'

import styles from './button.module.css'

export interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  secondary?: boolean
  children: ReactNode
}

const Button: FC<ButtonProps> = ({
  children,
  className,
  secondary,
  ...props
}) => {
  return (
    <button
      className={cn(styles.Button, className, {
        [styles['Button--secondary']]: secondary,
      })}
      {...props}
    >
      {children}
    </button>
  )
}

export interface LinkButtonProps
  extends AnchorHTMLAttributes<HTMLAnchorElement> {
  to: string
  secondary?: boolean
  children: ReactNode
}

export const LinkButton: FC<LinkButtonProps> = ({
  children,
  className,
  secondary,
  ...props
}) => {
  return (
    <Link
      className={cn(styles.Button, styles.LinkButton, className, {
        [styles['Button--secondary']]: secondary,
      })}
      {...props}
    >
      {children}
    </Link>
  )
}

export default Button
