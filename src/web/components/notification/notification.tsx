import { FC, useRef, useState } from 'react'
import { CSSTransition } from 'react-transition-group'
import cn from 'classnames'

import { Line } from '../text/text'

import styles from './notification.module.css'

const transitionClasses = {
  enterActive: styles.enterActive,
  enterDone: styles.enterDone,
  exit: styles.exit,
  exitActive: styles.exitActive,
}

export interface NotificationProps {
  visible: boolean
}

export const Notification: FC<NotificationProps> = ({ visible }) => {
  return (
    <CSSTransition
      in={visible}
      timeout={{ enter: 300, exit: 500 }}
      unmountOnExit
      classNames={transitionClasses}
    >
      <div className={cn(styles.Notification)}>
        <Line>Something went wrong, please try again later.</Line>
      </div>
    </CSSTransition>
  )
}

export const useNotification = () => {
  const [visible, setVisible] = useState(false)
  const timeoutRef = useRef<ReturnType<typeof setTimeout>>()

  const showNotification = () => {
    clearTimeout(timeoutRef.current)
    setVisible(true)
    timeoutRef.current = setTimeout(() => setVisible(false), 3000)
  }

  const notification = <Notification visible={visible} />

  return [notification, showNotification] as const
}
