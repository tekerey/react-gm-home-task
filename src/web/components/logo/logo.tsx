import { FC } from 'react'
import { Link } from 'react-router-dom'

import styles from './logo.module.css'

export const Logo: FC = () => {
  return (
    <span className={styles.Logo}>
      <span>netflix</span>
      <span>roulette</span>
    </span>
  )
}

export const LogoLink: FC = () => {
  return (
    <Link to="/search" className={styles.LogoLink}>
      <Logo />
    </Link>
  )
}
