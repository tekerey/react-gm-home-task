import userEvent from '@testing-library/user-event'

import { render, screen } from '../../utils/test-utils'
import Sorting from './sorting'

describe('<Sorting />', () => {
  const onSort = jest.fn()
  const value = 'release_date'
  const newValue = 'title'

  it('should render Sorting component', () => {
    const { asFragment } = render(<Sorting onSort={onSort} value={value} />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render sorting block', () => {
    render(<Sorting onSort={onSort} value={value} />)

    expect(screen.getByTestId('sorting-block')).toBeInTheDocument()
  })

  it('should render "Sort by" text', () => {
    render(<Sorting onSort={onSort} value={value} />)

    expect(screen.getByText(/sort by/i)).toBeInTheDocument()
  })

  it('should render select dropdown', () => {
    render(<Sorting onSort={onSort} value={value} />)

    expect(screen.getByRole('combobox')).toBeInTheDocument()
  })

  it('should render select icon', () => {
    render(<Sorting onSort={onSort} value={value} />)

    expect(screen.getByTestId('select-icon')).toBeInTheDocument()
  })

  it('should have release date as value if empty string passed as value', () => {
    render(<Sorting onSort={onSort} value="" />)

    expect(screen.getByRole('combobox')).toHaveValue('release_date')
  })

  it('should call callback with selected value', async () => {
    render(<Sorting onSort={onSort} value={value} />)

    const select = screen.getByRole('combobox')

    await userEvent.selectOptions(select, newValue)

    expect(onSort).toHaveBeenCalledTimes(1)
    expect(onSort).toHaveBeenCalledWith(newValue)
  })

  it('should have the correct value if value prop is passed', () => {
    render(<Sorting onSort={onSort} value={newValue} />)

    expect(screen.getByRole('combobox')).toHaveValue(newValue)
  })
})
