import userEvent from '@testing-library/user-event'

import { render, screen } from '../../utils/test-utils'

import FilterAndSortBar from './filterAndSortBar'

describe('<FilterAndSortBar />', () => {
  it('should render FilterAndSortBar component', () => {
    const { asFragment } = render(<FilterAndSortBar />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should have all as active filter by default', () => {
    render(<FilterAndSortBar />)

    expect(screen.getByText(/all/i)).toHaveClass('active')
  })

  it.each([
    { filter: /All/i },
    { filter: /Documentary/i },
    { filter: /Comedy/i },
    { filter: /Horror/i },
    { filter: /Crime/i },
  ])(
    'should change active filter to $filter when it is clicked',
    async ({ filter }) => {
      render(<FilterAndSortBar />)

      const filterButton = screen.getByText(filter)

      await userEvent.click(filterButton)

      expect(filterButton).toHaveClass('active')
    }
  )

  it('should change sorting when new value is selected', async () => {
    const value = 'release_date'
    const newValue = 'title'
    render(<FilterAndSortBar />)

    const select = screen.getByRole('combobox')
    expect(select).toHaveValue(value)

    await userEvent.selectOptions(select, newValue)

    expect(select).toHaveValue(newValue)
  })
})
