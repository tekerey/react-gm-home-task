import { FC } from 'react'
import { useSearchParams } from 'react-router-dom'
import cn from 'classnames'

import Button from '../button/button'
import Sorting from './sorting'

import styles from './filterAndSortBar.module.css'
import { Genre, GenreOrAll } from '../../../types/interfaces'

const genres: GenreOrAll[] = [
  'all',
  Genre.DOCUMENTARY,
  Genre.COMEDY,
  Genre.HORROR,
  Genre.CRIME,
]

const FilterAndSortBar: FC = () => {
  const [query, setQuery] = useSearchParams()

  const handleFiltering = (value: GenreOrAll) => {
    const newQuery = new URLSearchParams(query)

    if (value === 'all') {
      newQuery.delete('genre')
      setQuery(newQuery)
      return
    }

    newQuery.set('genre', value)
    setQuery(newQuery)
  }

  const handleSorting = (value: string) => {
    const newQuery = new URLSearchParams(query)
    newQuery.set('sortBy', value)
    setQuery(newQuery)
  }

  return (
    <div className={styles.FilterAndSortBar}>
      <div>
        <div className={styles.GenreFilter}>
          {genres.map(genre => (
            <Button
              key={genre}
              className={cn(styles.FilterAndSortBarButton, {
                [styles.active]:
                  query.get('genre') === genre ||
                  (genre === 'all' && !query.get('genre')),
              })}
              onClick={() => handleFiltering(genre)}
            >
              {genre}
            </Button>
          ))}
        </div>
        <Sorting onSort={handleSorting} value={query.get('sortBy')} />
      </div>
    </div>
  )
}

export default FilterAndSortBar
