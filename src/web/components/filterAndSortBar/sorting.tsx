import { ChangeEvent, FC } from 'react'
import SelectIcon from '../icons/selectIcon'

import styles from './sorting.module.css'

export interface SortingProps {
  onSort?: (value: string) => void
  value: string | null
}

const Sorting: FC<SortingProps> = ({ onSort, value }) => {
  const handleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    onSort?.(e.target.value)
  }

  return (
    <div className={styles.SortingBlock} data-test-id="sorting-block">
      <span>Sort by</span>
      <div className={styles.SortingSelectWrapper}>
        <select
          name="sorting"
          value={value || 'release_date'}
          className={styles.SortingSelect}
          onChange={handleChange}
        >
          <option value="release_date">Release date</option>
          <option value="title">Name</option>
          <option value="vote_average">Rating</option>
        </select>
        <SelectIcon className={styles.SortingSelectIcon} />
      </div>
    </div>
  )
}

export default Sorting
