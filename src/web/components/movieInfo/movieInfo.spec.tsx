import { movieMock } from '../../../mocks/movies'

import { render } from '../../utils/test-utils'

import MovieInfo from './movieInfo'

describe('<MovieInfo />', () => {
  it('should render MovieInfo component', () => {
    const { asFragment } = render(<MovieInfo movie={movieMock} />)

    expect(asFragment()).toMatchSnapshot()
  })
})
