import { FC } from 'react'

import { Movie } from '../../../types/interfaces'
import { Line, Title } from '../text/text'

import styles from './movieInfo.module.css'

export interface MovieInfoProps {
  movie: Movie
}

const MovieInfo: FC<MovieInfoProps> = ({ movie }) => {
  return (
    <div data-test-id="movie-info" className={styles.MovieInfoWrapper}>
      <img src={movie.poster_path} alt={movie.title} />
      <div className={styles.MovieInfo}>
        <div className={styles.MovieInfoHeader}>
          <Title
            data-test-id="movie-info-title"
            className={styles.MovieInfoTitle}
          >
            {movie.title}
          </Title>
          <span>{movie.vote_average}</span>
        </div>
        <Line className={styles.MovieInfoGenres}>
          {movie.genres.join(', ')}
        </Line>
        <div className={styles.MovieInfoStats}>
          <span>{movie.release_date.split('-')[0]}</span>
          <span>
            {Math.floor(movie.runtime / 60)}h {movie.runtime % 60}min
          </span>
        </div>
        <Line className={styles.MovieInfoDescription}>{movie.overview}</Line>
      </div>
    </div>
  )
}

export default MovieInfo
