import { ComponentStory, ComponentMeta } from '@storybook/react'

import { SearchBar } from './searchBar'

export default {
  title: 'Components/SearchBar',
  component: SearchBar,
} as ComponentMeta<typeof SearchBar>

const Template: ComponentStory<typeof SearchBar> = args => (
  <SearchBar {...args} />
)

export const Primary = Template.bind({})
Primary.args = {
  title: 'Find your movie',
  placeholder: 'What do you want to watch?',
}
