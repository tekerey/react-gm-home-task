import { ChangeEvent, FC, FormEvent, useEffect, useState } from 'react'

import Button from '../button/button'

import styles from './searchBar.module.css'

export interface SearchBarProps {
  title: string
  placeholder: string
  onSearch?: (value: string) => void
  search?: string
}

export const SearchBar: FC<SearchBarProps> = ({
  title,
  placeholder,
  onSearch,
  search = '',
}) => {
  const [value, setValue] = useState(search)

  const handleInput = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value)
  }

  const handleSubmit = (event: FormEvent<HTMLFormElement>) => {
    onSearch?.(value)
    event.preventDefault()
  }

  useEffect(() => {
    setValue(search)
  }, [search])

  return (
    <div data-test-id="search-bar" className={styles.SearchBarWrapper}>
      <h2 className={styles.SearchBarTitle}>{title}</h2>
      <form onSubmit={handleSubmit} className={styles.SearchBar}>
        <input
          type="search"
          placeholder={placeholder}
          value={value}
          onChange={handleInput}
        />
        <Button type="submit">Search</Button>
      </form>
    </div>
  )
}
