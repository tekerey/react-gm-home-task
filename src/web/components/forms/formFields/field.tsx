import { FC, InputHTMLAttributes, TextareaHTMLAttributes } from 'react'
import { useField } from 'formik'
import cn from 'classnames'

import { Dropdown, DropdownProps } from './dropdown'
import { Genre } from '../../../../types/interfaces'

import { FieldError } from './fieldError'

import styles from './field.module.css'

export interface FieldProps extends InputHTMLAttributes<HTMLInputElement> {
  label: string
  name: string
  icon?: JSX.Element
}

export const Field: FC<FieldProps> = ({ label, className, ...props }) => {
  const [field] = useField(props)

  return (
    <div className={cn(styles.Field, className)}>
      <label htmlFor={props.name}>{label}</label>
      <input id={props.name} {...field} {...props} />
      <FieldError name={field.name} />
    </div>
  )
}

export interface TextAreaProps
  extends TextareaHTMLAttributes<HTMLTextAreaElement> {
  label: string
  name: string
}

export const TextArea: FC<TextAreaProps> = ({ label, className, ...props }) => {
  const [field] = useField(props)

  return (
    <div className={cn(styles.Field, className)}>
      <label htmlFor={props.name}>{label}</label>
      <textarea id={props.name} {...field} {...props} />
      <FieldError name={field.name} />
    </div>
  )
}

export type DropdownFieldProps = Omit<
  DropdownProps,
  'selectedOptions' | 'onChange'
>

export const DropdownField: FC<DropdownFieldProps> = props => {
  const [field, _meta, helpers] = useField<Genre[]>(props.name)

  const handleChange = (selectedGenres: string[]) => {
    helpers.setValue(selectedGenres as Genre[])
  }

  return (
    <div>
      <Dropdown
        selectedOptions={field.value}
        onChange={handleChange}
        {...props}
      />
      <FieldError name={field.name} />
    </div>
  )
}
