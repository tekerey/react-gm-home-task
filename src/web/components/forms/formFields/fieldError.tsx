import { FC } from 'react'
import { ErrorMessage } from 'formik'

import styles from './fieldError.module.css'

export interface FieldErrorProps {
  name: string
}

export const FieldError: FC<FieldErrorProps> = ({ name }) => {
  return (
    <ErrorMessage name={name}>
      {msg => <span className={styles.FieldError}>{msg}</span>}
    </ErrorMessage>
  )
}
