import { ChangeEvent, FC, useState } from 'react'
import cn from 'classnames'

import styles from './field.module.css'
import SelectIcon from '../../icons/selectIcon'

export interface DropdownProps {
  label: string
  placeholder: string
  options: string[]
  selectedOptions: string[]
  name: string
  className?: string
  onChange?: (selectedOptions: string[]) => void
}

export const Dropdown: FC<DropdownProps> = ({
  label,
  placeholder,
  options,
  selectedOptions,
  className,
  onChange,
}) => {
  const [isOpen, setIsOpen] = useState<boolean>(false)

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const checked = e.target.checked
    const name = e.target.name

    if (checked) {
      onChange?.([...selectedOptions, name])
    } else {
      onChange?.(selectedOptions.filter(item => item !== name))
    }
  }

  return (
    <div className={cn(styles.Field, className)}>
      <label>{label}</label>
      <div className={styles.Dropdown} onPointerLeave={() => setIsOpen(false)}>
        <span data-test-id="dropdown" onClick={() => setIsOpen(open => !open)}>
          {selectedOptions.join(', ') || placeholder}
          <SelectIcon
            className={cn(styles.DropdownIcon, {
              [styles['DropdownIcon--open']]: isOpen,
            })}
          />
        </span>
        <div
          className={cn(styles.DropdownOptions, {
            [styles['DropdownOptions--open']]: isOpen,
          })}
        >
          {options.map(option => {
            return (
              <label key={option} className={styles.DropdownOption}>
                <input
                  type="checkbox"
                  name={option}
                  checked={selectedOptions.includes(option)}
                  onChange={handleChange}
                />
                {option}
              </label>
            )
          })}
        </div>
      </div>
    </div>
  )
}
