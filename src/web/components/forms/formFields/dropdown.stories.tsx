import { ComponentStory, ComponentMeta } from '@storybook/react'
import { Genre } from '../../../../types/interfaces'

import { Dropdown } from './dropdown'

export default {
  title: 'Components/Dropdown',
  component: Dropdown,
} as ComponentMeta<typeof Dropdown>

const Template: ComponentStory<typeof Dropdown> = args => <Dropdown {...args} />

export const Primary = Template.bind({})
Primary.args = {
  label: 'Genre',
  placeholder: 'Select items',
  options: [Genre.COMEDY, Genre.CRIME, Genre.DOCUMENTARY, Genre.HORROR],
  selectedOptions: ['Comedy', 'Horror'],
  name: 'genre',
}
