import userEvent from '@testing-library/user-event'

import { render, screen } from '../../utils/test-utils'

import MovieForm, { MovieFormProps } from './movieForm'

describe('<MovieForm />', () => {
  const title = 'Add movie'
  const onSubmit = jest.fn()
  const props = { title, onSubmit, isSubmitting: false } as MovieFormProps

  afterEach(() => {
    jest.clearAllMocks()
  })

  const fieldsTestCases = [
    { field: /Title/i, value: 'Star Wars: The Last Jedi' },
    { field: /Release Date/i, value: '2017-12-09' },
    {
      field: /Movie Url/i,
      value: 'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg',
    },
    { field: /Rating/i, value: 7 },
    { field: /Runtime/i, value: 152 },
    { field: /Overview/i, value: 'Some text here' },
  ]

  it('should render MovieForm component', () => {
    const { asFragment } = render(<MovieForm {...props} />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render a form', () => {
    render(<MovieForm {...props} />)

    expect(screen.getByRole('form')).toBeInTheDocument()
  })

  it('should render title', () => {
    render(<MovieForm {...props} />)

    expect(screen.getByText(title)).toBeInTheDocument()
  })

  it.each(fieldsTestCases)('should render $field form field', ({ field }) => {
    render(<MovieForm {...props} />)

    expect(screen.getByLabelText(field)).toBeInTheDocument()
  })

  it.each(fieldsTestCases)(
    'should change $field form field value to $value',
    async ({ field, value }) => {
      render(<MovieForm {...props} />)

      const formField = screen.getByLabelText(field)
      await userEvent.type(formField, value.toString())

      expect(formField).toHaveValue(value)
    }
  )

  it('should render reset button', () => {
    render(<MovieForm {...props} />)

    const button = screen.getByRole('button', { name: /reset/i })

    expect(button).toBeInTheDocument()
    expect(button).toBeEnabled()
  })

  it('should render disabled reset button if form is submitting', () => {
    render(<MovieForm {...props} isSubmitting />)

    const button = screen.getByRole('button', { name: /reset/i })

    expect(button).toBeInTheDocument()
    expect(button).toBeDisabled()
  })

  it('should render submit button', () => {
    render(<MovieForm {...props} />)

    const button = screen.getByRole('button', { name: /submit/i })

    expect(button).toBeInTheDocument()
    expect(button).toBeEnabled()
  })

  it('should render disabled submit button if form is submitting', () => {
    render(<MovieForm {...props} isSubmitting />)

    const button = screen.getByRole('button', { name: /submit/i })

    expect(button).toBeInTheDocument()
    expect(button).toBeDisabled()
  })

  it('should NOT fire callback on form submit if form is not filled', async () => {
    render(<MovieForm {...props} />)

    await userEvent.click(screen.getByRole('button', { name: /submit/i }))

    expect(onSubmit).not.toHaveBeenCalled()
  })

  it('should fire callback on form submit', async () => {
    render(<MovieForm {...props} />)

    await userEvent.click(screen.getByTestId('dropdown'))
    await userEvent.click(screen.getByRole('checkbox', { name: 'Comedy' }))
    await userEvent.click(screen.getByRole('checkbox', { name: 'Crime' }))

    await userEvent.type(screen.getByLabelText(/Title/i), 'Some title')
    await userEvent.type(screen.getByLabelText(/Release Date/i), '2017-12-09')
    await userEvent.type(
      screen.getByLabelText(/Movie Url/i),
      'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg'
    )
    await userEvent.type(screen.getByLabelText(/Rating/i), '7')
    await userEvent.type(screen.getByLabelText(/Runtime/i), '152')
    await userEvent.type(screen.getByLabelText(/Overview/i), 'Some text here')

    await userEvent.click(screen.getByRole('button', { name: /submit/i }))

    expect(onSubmit).toHaveBeenCalledTimes(1)
    expect(onSubmit).toHaveBeenCalledWith({
      genres: ['Comedy', 'Crime'],
      movieUrl:
        'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg',
      overview: 'Some text here',
      rating: 7,
      releaseDate: '2017-12-09',
      runtime: 152,
      title: 'Some title',
    })
  })
})
