import { FC } from 'react'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'

import { MovieFormData } from '../../../types/interfaces'
import { validation } from '../../../utils/validators'

import Button from '../button/button'
import { Title } from '../text/text'

import { DropdownField, Field, TextArea } from './formFields/field'

import styles from './movieForm.module.css'

export interface MovieFormProps {
  title: string
  onSubmit: (data: MovieFormData) => Promise<void>
  isSubmitting: boolean
  initialFormData?: MovieFormData
}

const defaultFormData: MovieFormData = {
  title: '',
  releaseDate: '',
  movieUrl: '',
  rating: '',
  genres: [],
  runtime: '',
  overview: '',
}

const MovieForm: FC<MovieFormProps> = ({
  title,
  onSubmit,
  isSubmitting,
  initialFormData = defaultFormData,
}) => {
  return (
    <Formik
      initialValues={initialFormData}
      validationSchema={Yup.object().shape(validation)}
      onSubmit={values => {
        onSubmit?.(values)
      }}
    >
      <Form aria-label="movie-form">
        <Title>{title}</Title>
        <div className={styles.FormFieldsWrapper}>
          <Field label="Title" name="title" />
          <Field
            type="date"
            label="Release Date"
            name="releaseDate"
            placeholder="Select Date"
          />
          <Field
            type="url"
            label="Movie Url"
            name="movieUrl"
            placeholder="https://"
          />
          <Field
            type="number"
            label="Rating"
            name="rating"
            placeholder="7.8"
            min={0}
            max={10}
          />
          <DropdownField
            label="Genre"
            name="genres"
            placeholder="Select Genre"
            options={['Crime', 'Documentary', 'Horror', 'Comedy']}
          />
          <Field
            type="number"
            label="Runtime"
            name="runtime"
            placeholder="minutes"
            min={0}
          />
          <TextArea
            label="Overview"
            name="overview"
            className={styles.FullWidthField}
            placeholder="Movie description"
          />
        </div>
        <div className={styles.Buttons}>
          <Button type="reset" secondary disabled={isSubmitting}>
            Reset
          </Button>
          <Button type="submit" disabled={isSubmitting}>
            Submit
          </Button>
        </div>
      </Form>
    </Formik>
  )
}

export default MovieForm
