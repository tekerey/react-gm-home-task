import { Component, ReactNode } from 'react'

import styles from './errorBoundary.module.css'

export interface ErrorBoundaryProps {
  children?: ReactNode
}

export interface ErrorBoundaryState {
  hasError: boolean
}

export default class ErrorBoundary extends Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  state = {
    hasError: false,
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  render() {
    if (this.state.hasError) {
      return (
        <div className={styles.ErrorPage}>
          <h1>Oops! Something went wrong!</h1>
          <p>Please refresh the page or try later.</p>
        </div>
      )
    }

    return this.props.children
  }
}
