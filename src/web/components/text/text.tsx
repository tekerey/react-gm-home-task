import { FC, ReactNode } from 'react'
import cn from 'classnames'

import styles from './text.module.css'

export interface TextProps {
  children: ReactNode
  className?: string
  'data-test-id'?: string
  center?: boolean
  removeBottomPadding?: boolean
}

export const Title: FC<TextProps> = ({
  children,
  className,
  'data-test-id': dataTestId,
}) => (
  <h1
    data-test-id={dataTestId}
    className={cn(styles.Text, styles.Title, className)}
  >
    {children}
  </h1>
)

export const Line: FC<TextProps> = ({
  children,
  className,
  center,
  removeBottomPadding,
}) => (
  <p
    className={cn(styles.Text, styles.Line, className, {
      [styles['Text--center']]: center,
      [styles['Text--removeBottomPadding']]: removeBottomPadding,
    })}
  >
    {children}
  </p>
)
