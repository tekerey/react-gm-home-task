import { FC } from 'react'
import { CSSTransition } from 'react-transition-group'

import styles from './loader.module.css'

export const Loader: FC = () => {
  return (
    <div className={styles.Loader}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}

export const LoaderWithWrapper: FC = () => {
  return (
    <div data-test-id="loader-with-wrapper" className={styles.LoaderWrapper}>
      <Loader />
    </div>
  )
}

export interface LoaderOverlayProps {
  visible?: boolean
}

const transitionClasses = {
  enterActive: styles.LoaderOverlayEnterActive,
  enterDone: styles.LoaderOverlayEnterDone,
  exit: styles.LoaderOverlayExit,
  exitActive: styles.LoaderOverlayExitActive,
}

export const LoaderOverlay: FC<LoaderOverlayProps> = ({ visible = false }) => {
  return (
    <CSSTransition
      in={visible}
      timeout={300}
      unmountOnExit
      classNames={transitionClasses}
    >
      <div className={styles.LoaderOverlay}>
        <LoaderWithWrapper />
      </div>
    </CSSTransition>
  )
}
