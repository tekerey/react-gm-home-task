import userEvent from '@testing-library/user-event'
import { useDispatch } from 'react-redux'

import { modalActions } from '../../../core/modals'
import { render, screen } from '../../utils/test-utils'
import { movieMock } from '../../../mocks/movies'

import { MovieTile } from './movieTile'
import { Modal } from '../modals/modals'

jest.mock('react-redux', () => ({
  ...jest.requireActual('react-redux'),
  useDispatch: jest.fn(),
}))

describe('<MovieTile />', () => {
  const onClick = jest.fn()
  const dispatch = jest.fn()

  jest.mocked(useDispatch).mockReturnValue(dispatch)

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should render MovieTile component', () => {
    const { asFragment } = render(
      <MovieTile movie={movieMock} onClick={onClick} />
    )

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render nothing if no movie was passed', () => {
    const { container } = render(<MovieTile onClick={onClick} />)

    expect(container).toBeEmptyDOMElement()
  })

  it('should call callback on click', async () => {
    render(<MovieTile movie={movieMock} onClick={onClick} />)

    await userEvent.click(screen.getByTestId('movie-tile'))

    expect(onClick).toHaveBeenCalledTimes(1)
  })

  it('should invoke opening edit movie modal', async () => {
    render(<MovieTile movie={movieMock} onClick={onClick} />)

    await userEvent.click(screen.getByRole('button', { name: 'Edit' }))

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch).toHaveBeenCalledWith(
      modalActions.openModal({
        modal: Modal.EDIT_MOVIE,
        movieForModal: movieMock,
      })
    )
  })

  it('should invoke opening delete movie modal', async () => {
    render(<MovieTile movie={movieMock} onClick={onClick} />)

    await userEvent.click(screen.getByRole('button', { name: 'Delete' }))

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch).toHaveBeenCalledWith(
      modalActions.openModal({
        modal: Modal.DELETE_MOVIE,
        movieForModal: movieMock,
      })
    )
  })

  it('should NOT show context menu by default', () => {
    render(<MovieTile movie={movieMock} onClick={onClick} />)

    expect(screen.getByTestId('movie-tile-context-menu')).not.toHaveClass(
      'MovieTileContextMenu--open'
    )
  })

  it('should show context menu on click', async () => {
    render(<MovieTile movie={movieMock} onClick={onClick} />)

    await userEvent.click(screen.getByTestId('show-movie-tile-context-menu'))

    expect(screen.getByTestId('movie-tile-context-menu')).toHaveClass(
      'MovieTileContextMenu--open'
    )
  })

  it('should close context menu on close button click', async () => {
    render(<MovieTile movie={movieMock} onClick={onClick} />)

    await userEvent.click(screen.getByTestId('show-movie-tile-context-menu'))

    expect(screen.getByTestId('movie-tile-context-menu')).toHaveClass(
      'MovieTileContextMenu--open'
    )

    await userEvent.click(screen.getByTestId('close-movie-tile-context-menu'))

    expect(screen.getByTestId('movie-tile-context-menu')).not.toHaveClass(
      'MovieTileContextMenu--open'
    )
  })
})
