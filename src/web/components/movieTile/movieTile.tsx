import { FC, useState } from 'react'
import { useDispatch } from 'react-redux'
import cn from 'classnames'

import { modalActions } from '../../../core/modals'
import { Movie } from '../../../types/interfaces'

import { SettingsIcon } from '../icons/settingsIcon'

import styles from './movieTile.module.css'
import CloseButton from '../button/closeButton'
import { Modal } from '../modals/modals'
import { QueryLink } from '../links/queryLink'
import { MovieImage } from '../movieImage/movieImage'

export interface MovieTileProps {
  movie?: Movie
  onClick?: () => void
}

export const MovieTile: FC<MovieTileProps> = ({ movie, onClick }) => {
  const dispatch = useDispatch()

  const [showContextMenu, setShowContextMenu] = useState(false)

  if (!movie) return null

  const handleClick = () => {
    onClick?.()
    scrollTo({ top: 0, behavior: 'smooth' })
  }

  const handleEdit = () =>
    dispatch(
      modalActions.openModal({
        modal: Modal.EDIT_MOVIE,
        movieForModal: movie,
      })
    )
  const handleDelete = () =>
    dispatch(
      modalActions.openModal({
        modal: Modal.DELETE_MOVIE,
        movieForModal: movie,
      })
    )

  return (
    <div className={styles.MovieTileWrapper}>
      <QueryLink
        data-test-id="movie-tile"
        className={styles.MovieTile}
        onClick={handleClick}
        queryAction="set"
        queryKey="movie"
        queryValue={movie.id.toString()}
      >
        <MovieImage src={movie.poster_path} alt={movie.title} />
        <div className={styles.MovieTileTitleAndYear}>
          <span className={styles.MovieTileTitle}>{movie.title}</span>
          <span className={styles.MovieTileYear}>
            {movie.release_date.split('-')[0]}
          </span>
        </div>
        <span className={styles.MovieTileDescription}>
          {movie.tagline || movie.genres.join(', ')}
        </span>
      </QueryLink>
      <div className={styles.MovieTileActions}>
        <button
          data-test-id="show-movie-tile-context-menu"
          onClick={() => setShowContextMenu(true)}
          className={styles.MovieTileSettingsButton}
        >
          <SettingsIcon />
        </button>
        <div
          data-test-id="movie-tile-context-menu"
          className={cn(styles.MovieTileContextMenu, {
            [styles['MovieTileContextMenu--open']]: showContextMenu,
          })}
          onPointerLeave={() => setShowContextMenu(false)}
        >
          <CloseButton
            data-test-id="close-movie-tile-context-menu"
            size="small"
            className={styles.MovieTileContextMenuClose}
            onClick={() => setShowContextMenu(false)}
          />
          <button onClick={handleEdit}>Edit</button>
          <button onClick={handleDelete}>Delete</button>
        </div>
      </div>
    </div>
  )
}
