import { FC, ReactNode } from 'react'
import { Link, LinkProps, useLocation, useSearchParams } from 'react-router-dom'

export interface QueryLinkOwnProps {
  queryKey: string
  children: ReactNode
}

export type QueryLinkProps = QueryLinkOwnProps &
  Omit<LinkProps, 'to'> &
  (
    | { queryAction: 'set'; queryValue: string }
    | { queryAction: 'delete'; queryValue?: never }
  )

export const QueryLink: FC<QueryLinkProps> = ({
  children,
  queryKey,
  queryValue,
  queryAction,
  ...props
}) => {
  const [query] = useSearchParams()
  const location = useLocation()

  const newQuery = new URLSearchParams(query)

  queryAction === 'set'
    ? newQuery.set(queryKey, queryValue)
    : newQuery.delete(queryKey)

  const to = `${location.pathname}?${newQuery.toString()}`

  return (
    <Link to={to} {...props}>
      {children}
    </Link>
  )
}
