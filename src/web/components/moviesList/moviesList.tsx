import { FC, memo } from 'react'

import { Movie } from '../../../types/interfaces'

import { MovieTile } from '../movieTile/movieTile'

import styles from './moviesList.module.css'

export interface MoviesListProps {
  movies: Movie[]
}

const MoviesList: FC<MoviesListProps> = ({ movies }) => {
  return (
    <div data-test-id="movies-list" className={styles.MoviesList}>
      {movies.map(movie => (
        <MovieTile key={movie.id} movie={movie} />
      ))}
    </div>
  )
}

export default memo(MoviesList)
