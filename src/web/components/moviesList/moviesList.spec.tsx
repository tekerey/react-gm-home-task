import { renderWithStore } from '../../utils/test-utils'
import { moviesMock } from '../../../mocks/movies'

import MoviesList from './moviesList'

describe('<MoviesList />', () => {
  it('should render MoviesList component', () => {
    const { asFragment } = renderWithStore(<MoviesList movies={moviesMock} />)

    expect(asFragment()).toMatchSnapshot()
  })
})
