import { FC } from 'react'

import styles from './movieImage.module.css'

export interface MovieImageProps {
  src: string
  alt: string
}

export const MovieImage: FC<MovieImageProps> = ({ src, alt }) => (
  <div className={styles.MovieImageWrapper}>
    <img src={src} alt={alt} className={styles.MovieImage} />
  </div>
)
