import { movieFormDataMock } from '../../../mocks/movies'

import { renderWithStore } from '../../utils/test-utils'

import MovieForm from '../forms/movieForm'

import AddMovieModal from './addMovieModal'

jest.mock('../forms/movieForm', () => ({
  __esModule: true,
  default: jest.fn(),
}))

describe('<AddMovieModal />', () => {
  jest.mocked(MovieForm).mockImplementation(({ onSubmit }) => (
    <div>
      <h1>MovieForm</h1>
      <button onClick={() => onSubmit(movieFormDataMock)}>submit</button>
    </div>
  ))

  it('should render AddMovieModal component', () => {
    const { asFragment } = renderWithStore(<AddMovieModal />)

    expect(asFragment()).toMatchSnapshot()
  })
})
