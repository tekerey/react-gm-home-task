import { FC } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { MovieFormData } from '../../../types/interfaces'

import { useUpdateMovieMutation } from '../../../core/movies'
import { selectMovieForModal, modalActions } from '../../../core/modals'
import { formDataToMovie, movieToFormData } from '../../../utils'

import ModalWrapper from '../../containers/modalWrapper/modalWrapper'
import MovieForm from '../forms/movieForm'
import { LoaderOverlay } from '../loader/loader'
import { useNotification } from '../notification/notification'

import modalStyles from './modals.module.css'

const EditMovieModal: FC = () => {
  const dispatch = useDispatch()

  const movie = useSelector(selectMovieForModal)

  const [updateMovie, { isLoading }] = useUpdateMovieMutation()

  const [notification, showNotification] = useNotification()

  if (!movie) return null

  const handleSubmit = async (formData: MovieFormData) => {
    try {
      await updateMovie(formDataToMovie(formData, movie.id)).unwrap()
      dispatch(modalActions.closeModal())
    } catch (error) {
      showNotification()
    }
  }

  return (
    <>
      {notification}
      <LoaderOverlay visible={isLoading} />
      <ModalWrapper className={modalStyles.AddMovieModal}>
        <MovieForm
          title="Edit movie"
          initialFormData={movieToFormData(movie)}
          onSubmit={handleSubmit}
          isSubmitting={isLoading}
        />
      </ModalWrapper>
    </>
  )
}

export default EditMovieModal
