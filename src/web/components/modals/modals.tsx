import { FC } from 'react'

import AddMovieModal from './addMovieModal'
import { AddMovieSuccessModal } from './addMovieSuccessModal'
import DeleteMovieModal from './deleteMovieModal'
import EditMovieModal from './editMovieModal'

export enum Modal {
  ADD_MOVIE = 'addMovie',
  EDIT_MOVIE = 'editMovie',
  DELETE_MOVIE = 'deleteMovie',
  ADD_MOVIE_SUCCESS = 'addMovieSuccess',
}

export const modals: Partial<Record<Modal, FC>> = {
  [Modal.ADD_MOVIE]: AddMovieModal,
  [Modal.EDIT_MOVIE]: EditMovieModal,
  [Modal.DELETE_MOVIE]: DeleteMovieModal,
  [Modal.ADD_MOVIE_SUCCESS]: AddMovieSuccessModal,
}
