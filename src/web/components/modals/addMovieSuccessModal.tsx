import { FC } from 'react'

import ModalWrapper from '../../containers/modalWrapper/modalWrapper'

import { SuccessIcon } from '../icons/successIcon'
import { Line, Title } from '../text/text'

import modalStyles from './modals.module.css'

export const AddMovieSuccessModal: FC = () => {
  return (
    <ModalWrapper className={modalStyles.AddMovieSuccessModal}>
      <SuccessIcon className={modalStyles.AddMovieSuccessModalIcon} />
      <Title>Congratulations !</Title>
      <Line center removeBottomPadding>
        The movie has been added to
        <br />
        database successfully
      </Line>
    </ModalWrapper>
  )
}
