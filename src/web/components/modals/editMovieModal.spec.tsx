import { RootState } from '../../../core'
import { movieFormDataMock, movieMock } from '../../../mocks/movies'

import { renderWithStore } from '../../utils/test-utils'

import MovieForm from '../forms/movieForm'

import EditMovieModal from './editMovieModal'

jest.mock('../forms/movieForm', () => ({
  __esModule: true,
  default: jest.fn(),
}))

describe('<EditMovieModal />', () => {
  jest.mocked(MovieForm).mockImplementation(({ onSubmit }) => (
    <div>
      <h1>MovieForm</h1>
      <button onClick={() => onSubmit(movieFormDataMock)}>submit</button>
    </div>
  ))

  it('should render EditMovieModal component if there is movie for modal in store', () => {
    const { asFragment } = renderWithStore(<EditMovieModal />, {
      preloadedState: {
        modals: {
          movieForModal: movieMock,
        },
      } as RootState,
    })

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render nothing if there is no movie for modal in store', () => {
    const { container } = renderWithStore(<EditMovieModal />)

    expect(container).toBeEmptyDOMElement()
  })
})
