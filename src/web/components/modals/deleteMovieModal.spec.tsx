import { RootState } from '../../../core'
import { movieMock } from '../../../mocks/movies'

import { renderWithStore } from '../../utils/test-utils'

import DeleteMovieModal from './deleteMovieModal'

describe('<DeleteMovieModal />', () => {
  it('should render DeleteMovieModal component if there is movie for modal in store', () => {
    const { asFragment } = renderWithStore(<DeleteMovieModal />, {
      preloadedState: {
        modals: {
          movieForModal: movieMock,
        },
      } as RootState,
    })

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render nothing if there is no movie for modal in store', () => {
    const { container } = renderWithStore(<DeleteMovieModal />)

    expect(container).toBeEmptyDOMElement()
  })
})
