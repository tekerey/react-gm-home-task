import { FC } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useSearchParams } from 'react-router-dom'

import { selectMovieForModal, modalActions } from '../../../core/modals'
import { useDeleteMovieMutation } from '../../../core/movies'

import ModalWrapper from '../../containers/modalWrapper/modalWrapper'

import Button from '../button/button'
import { LoaderOverlay } from '../loader/loader'
import { useNotification } from '../notification/notification'
import { Line, Title } from '../text/text'

import modalStyles from './modals.module.css'

const DeleteMovieModal: FC = () => {
  const dispatch = useDispatch()

  const movie = useSelector(selectMovieForModal)
  const [deleteMovie, { isLoading }] = useDeleteMovieMutation()
  const [query, setQuery] = useSearchParams()

  const [notification, showNotification] = useNotification()

  if (!movie) return null

  const handleDelete = async () => {
    try {
      await deleteMovie(movie.id).unwrap()
      dispatch(modalActions.closeModal())

      if (movie.id.toString() === query.get('movie')) {
        const newQuery = new URLSearchParams(query)
        newQuery.delete('movie')
        setQuery(newQuery)
      }
    } catch (error) {
      showNotification()
    }
  }

  return (
    <>
      {notification}
      <LoaderOverlay visible={isLoading} />
      <ModalWrapper className={modalStyles.DeleteMovieModal}>
        <Title>Delete movie</Title>
        <Line>Are you sure you want to delete this movie?</Line>
        <div className={modalStyles.DeleteButtonWrapper}>
          <Button onClick={handleDelete}>Confirm</Button>
        </div>
      </ModalWrapper>
    </>
  )
}

export default DeleteMovieModal
