import { FC } from 'react'
import { useDispatch } from 'react-redux'

import { MovieFormData } from '../../../types/interfaces'

import { useAddMovieMutation } from '../../../core/movies'
import { modalActions } from '../../../core/modals'
import { formDataToCreateMovieDTO } from '../../../utils'

import ModalWrapper from '../../containers/modalWrapper/modalWrapper'
import MovieForm from '../forms/movieForm'
import { LoaderOverlay } from '../loader/loader'
import { useNotification } from '../notification/notification'

import { Modal } from './modals'

import modalStyles from './modals.module.css'

const AddMovieModal: FC = () => {
  const dispatch = useDispatch()

  const [addMovie, { isLoading }] = useAddMovieMutation()

  const [notification, showNotification] = useNotification()

  const handleSubmit = async (formData: MovieFormData) => {
    try {
      await addMovie(formDataToCreateMovieDTO(formData)).unwrap()
      dispatch(modalActions.openModal({ modal: Modal.ADD_MOVIE_SUCCESS }))
    } catch (error) {
      showNotification()
    }
  }

  return (
    <>
      {notification}
      <LoaderOverlay visible={isLoading} />
      <ModalWrapper className={modalStyles.AddMovieModal}>
        <MovieForm
          title="Add movie"
          onSubmit={handleSubmit}
          isSubmitting={isLoading}
        />
      </ModalWrapper>
    </>
  )
}

export default AddMovieModal
