import { renderWithStore } from '../../utils/test-utils'

import { AddMovieSuccessModal } from './addMovieSuccessModal'

describe('<AddMovieSuccessModal />', () => {
  it('should render AddMovieSuccessModal component', () => {
    const { asFragment } = renderWithStore(<AddMovieSuccessModal />)

    expect(asFragment()).toMatchSnapshot()
  })
})
