import { FC } from 'react'

import styles from './notFoundView.module.css'

import { Title, Line } from '../../components/text/text'
import { LinkButton } from '../../components/button/button'

export const NotFoundView: FC = () => (
  <div className={styles.NotFoundView}>
    <Title>Page not found</Title>
    <Line>There is nothing here.</Line>
    <LinkButton to="/search">Back to search</LinkButton>
  </div>
)
