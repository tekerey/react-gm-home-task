import { FC } from 'react'

import Button from '../../components/button/button'
import { Title } from '../../components/text/text'

import styles from './errorView.module.css'

export interface ErrorViewProps {
  refetch: () => void
}

export const ErrorView: FC<ErrorViewProps> = ({ refetch }) => {
  return (
    <div className={styles.ErrorView}>
      <Title>We could not load movies, please try again later.</Title>
      <Button onClick={() => refetch()}>Retry</Button>
    </div>
  )
}
