import { FC } from 'react'

import { useGetMoviesQueryWithParams } from '../../hooks/useGetMoviesQueryWithParams'

import { ErrorView } from '../errorView/errorView'
import {
  LoaderOverlay,
  LoaderWithWrapper,
} from '../../components/loader/loader'
import FilterAndSortBar from '../../components/filterAndSortBar/filterAndSortBar'
import MoviesList from '../../components/moviesList/moviesList'

import styles from './moviesListView.module.css'

const MoviesListView: FC = () => {
  const {
    data: movies,
    error,
    isLoading,
    isFetching,
    refetch,
  } = useGetMoviesQueryWithParams()

  if (isLoading) return <LoaderWithWrapper />

  return (
    <>
      <LoaderOverlay visible={isFetching} />
      <FilterAndSortBar />
      {error || !movies ? (
        <ErrorView refetch={refetch} />
      ) : (
        <>
          <p className={styles.TotalMovies}>
            <span>{movies.length}</span> movies found
          </p>
          <MoviesList movies={movies} />
        </>
      )}
    </>
  )
}

export default MoviesListView
