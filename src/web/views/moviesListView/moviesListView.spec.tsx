import { renderWithStore, screen } from '../../utils/test-utils'

import MoviesListView from './moviesListView'

describe('<MoviesListView />', () => {
  it('should render MoviesListView component with a loader', () => {
    const { asFragment } = renderWithStore(<MoviesListView />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render MoviesListView component with a movies list', async () => {
    const { asFragment } = renderWithStore(<MoviesListView />)

    await screen.findByTestId('movies-list')

    expect(asFragment()).toMatchSnapshot()
  })
})
