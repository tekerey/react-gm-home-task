import { configure, render as rtlRender } from '@testing-library/react'
import { ReactElement } from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import { setupStore } from '../../core'

configure({ testIdAttribute: 'data-test-id' })

export * from '@testing-library/react'

export const render = (ui: ReactElement, { ...renderOptions } = {}) => {
  return rtlRender(ui, { wrapper: BrowserRouter, ...renderOptions })
}

export const renderWithStore = (
  ui: ReactElement,
  {
    preloadedState = {},
    store = setupStore(preloadedState),
    route = '/',
    ...renderOptions
  } = {}
) => {
  window.history.pushState({}, '', route)

  const Wrapper = ({ children }) => (
    <Provider store={store}>
      <BrowserRouter>{children}</BrowserRouter>
    </Provider>
  )

  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions })
}
