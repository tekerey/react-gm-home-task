import { FC } from 'react'

import { RootState } from '../core'

export interface HtmlProps {
  contentString: string
  preloadedState: RootState
  manifest: Record<string, string>
}

const getScript = (script: string) =>
  script ? <script defer src={'/static' + script}></script> : null

const getStyles = (styles: string) =>
  styles ? <link href={'/static' + styles} rel="stylesheet" /> : null

export const Html: FC<HtmlProps> = ({
  contentString,
  preloadedState,
  manifest,
}) => {
  return (
    <html lang="en">
      <head>
        <meta charSet="UTF-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta
          name="description"
          content="Watch NetflixRoulette movies & TV shows online or stream right to your smart TV, game console, PC, Mac, mobile, tablet and more."
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link
          rel="preconnect"
          href="https://fonts.gstatic.com"
          crossOrigin="true"
        />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
          rel="stylesheet"
        />
        <title>
          NetflixRoulette - Watch TV Shows Online, Watch Movies Online
        </title>
        {getScript(manifest['runtime.js'])}
        {getScript(manifest['vendors.js'])}
        {getScript(manifest['main.js'])}
        {getStyles(manifest['styles.css'])}
      </head>
      <body>
        <div id="app" dangerouslySetInnerHTML={{ __html: contentString }}></div>
        <script
          dangerouslySetInnerHTML={{
            __html: `window.__state=${JSON.stringify(preloadedState)};`,
          }}
          charSet="UTF-8"
        />
      </body>
    </html>
  )
}
