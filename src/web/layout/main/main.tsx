import { FC, ReactNode } from 'react'

import styles from './main.module.css'

export interface MainProps {
  children: ReactNode
}

const Main: FC<MainProps> = ({ children }) => {
  return (
    <main className={styles.Main}>
      <div className={styles.MainContent}>{children}</div>
    </main>
  )
}

export default Main
