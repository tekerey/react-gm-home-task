import { FC, useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate, useParams, useSearchParams } from 'react-router-dom'

import { modalActions } from '../../../core/modals'
import { useGetMovieQuery } from '../../../core/movies'

import Button from '../../components/button/button'
import { SearchIcon } from '../../components/icons/searchIcon'
import { QueryLink } from '../../components/links/queryLink'
import { LoaderOverlay } from '../../components/loader/loader'
import { LogoLink } from '../../components/logo/logo'
import { Modal } from '../../components/modals/modals'
import MovieInfo from '../../components/movieInfo/movieInfo'
import { useNotification } from '../../components/notification/notification'
import { SearchBar } from '../../components/searchBar/searchBar'
import HeaderTopLine from '../../containers/headerTopLine/headerTopLine'
import HeaderWrapper from '../../containers/headerWrapper/headerWrapper'

const Header: FC = () => {
  const dispatch = useDispatch()
  const [notification, showErrorNotification] = useNotification()

  const navigate = useNavigate()
  const { searchQuery } = useParams()
  const [query] = useSearchParams()

  const openAddMovieModal = () => {
    dispatch(
      modalActions.openModal({
        modal: Modal.ADD_MOVIE,
      })
    )
  }

  const handleSearch = (value: string) => {
    navigate(`/search/${value}`)
  }

  const selectedMovieId = query.get('movie') || ''

  const {
    data: movie,
    isFetching,
    error,
  } = useGetMovieQuery(selectedMovieId, {
    skip: !selectedMovieId,
  })

  const selectedMovie = selectedMovieId ? movie : null

  useEffect(() => {
    if (error) showErrorNotification()
  }, [error])

  return (
    <>
      {notification}
      <LoaderOverlay visible={isFetching} />
      <HeaderWrapper showBackgroundImage={!selectedMovie}>
        <HeaderTopLine>
          <LogoLink />
          {selectedMovie ? (
            <QueryLink
              queryAction="delete"
              queryKey="movie"
              className="SearchButton"
            >
              <SearchIcon />
            </QueryLink>
          ) : (
            <Button className="AddButton" onClick={openAddMovieModal}>
              + Add movie
            </Button>
          )}
        </HeaderTopLine>
        {selectedMovie ? (
          <MovieInfo movie={selectedMovie} />
        ) : (
          <SearchBar
            title="Find your movie"
            placeholder="What do you want to watch?"
            onSearch={handleSearch}
            search={searchQuery}
          />
        )}
      </HeaderWrapper>
    </>
  )
}

export default Header
