import { renderWithStore, screen } from '../../utils/test-utils'

import Header from './header'

jest.mock('../../components/movieInfo/movieInfo', () => ({
  __esModule: true,
  default: jest.fn(() => <div>MovieInfo</div>),
}))

describe('<Header />', () => {
  it('should render Header component with a search bar by default', () => {
    const { asFragment } = renderWithStore(<Header />)

    expect(asFragment()).toMatchSnapshot()
  })

  it('should render Header component with a movie details', async () => {
    const { asFragment } = renderWithStore(<Header />, {
      route: '/search?movie=123456',
    })

    await screen.findByText('MovieInfo')

    expect(asFragment()).toMatchSnapshot()
  })
})
