import { FC, ReactNode } from 'react'

import styles from './footer.module.css'

export interface FooterProps {
  children: ReactNode
}

const Footer: FC<FooterProps> = ({ children }) => (
  <footer className={styles.Footer}>{children}</footer>
)

export default Footer
