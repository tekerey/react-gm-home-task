import { FC, useEffect } from 'react'
import { useSelector } from 'react-redux'

import { selectModal } from '../../../core/modals'

import { modals } from '../../components/modals/modals'

import styles from './modalOverlay.module.css'

const ModalOverlay: FC = () => {
  const modal = useSelector(selectModal)

  useEffect(() => {
    if (modal) {
      document.body.style.overflow = 'hidden'
      return
    }

    document.body.style.overflow = 'auto'
  }, [modal])

  if (!modal) return null

  const ModalComponent = modals[modal]
  if (!ModalComponent) return null

  return (
    <div className={styles.ModalOverlay}>
      <ModalComponent />
    </div>
  )
}

export default ModalOverlay
