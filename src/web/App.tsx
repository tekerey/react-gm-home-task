import { FC } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

import Footer from './layout/footer/footer'
import Header from './layout/header/header'
import Main from './layout/main/main'
import ModalOverlay from './layout/modalOverlay/modalOverlay'

import ErrorBoundary from './components/errorBoundary/errorBoundary'
import { Logo } from './components/logo/logo'
import MoviesListView from './views/moviesListView/moviesListView'
import { NotFoundView } from './views/notFoundView/notFoundView'

const App: FC = () => {
  return (
    <>
      <Routes>
        <Route path="/search" element={<Header />} />
        <Route path="/search/:searchQuery" element={<Header />} />
      </Routes>
      <Main>
        <ErrorBoundary>
          <Routes>
            <Route path="/" element={<Navigate to="search" replace />} />
            <Route path="/search" element={<MoviesListView />} />
            <Route path="/search/:searchQuery" element={<MoviesListView />} />
            <Route path="*" element={<NotFoundView />} />
          </Routes>
        </ErrorBoundary>
      </Main>
      <Footer>
        <Logo />
      </Footer>
      <ModalOverlay />
    </>
  )
}

export default App
