import { FC, ReactNode } from 'react'

import styles from './headerWrapper.module.css'

export interface HeaderWrapperProps {
  children: ReactNode
  showBackgroundImage?: boolean
}

const HeaderWrapper: FC<HeaderWrapperProps> = ({
  children,
  showBackgroundImage = true,
}) => {
  return (
    <header className={styles.HeaderWrapper}>
      {showBackgroundImage && (
        <div className={styles.HeaderBackgroundImage}></div>
      )}
      <div className={styles.HeaderContent}>{children}</div>
    </header>
  )
}

export default HeaderWrapper
