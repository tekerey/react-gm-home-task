import { FC, ReactNode } from 'react'
import { useDispatch } from 'react-redux'
import cn from 'classnames'

import { modalActions } from '../../../core/modals'

import CloseButton from '../../components/button/closeButton'
import { useOutsideClick } from '../../hooks/useOutsideClick'

import styles from './modalWrapper.module.css'

export interface ModalWrapperProps {
  className?: string
  children: ReactNode
}

const ModalWrapper: FC<ModalWrapperProps> = ({ children, className }) => {
  const dispatch = useDispatch()

  const closeModal = () => dispatch(modalActions.closeModal())

  const ref = useOutsideClick<HTMLDivElement>(closeModal)

  return (
    <div ref={ref} className={cn(styles.ModalWrapper, className)}>
      {children}
      <CloseButton
        onClick={closeModal}
        className={styles.ModalWrapperCloseButton}
      />
    </div>
  )
}

export default ModalWrapper
