import { FC, ReactNode } from 'react'

import styles from './headerTopLine.module.css'

export interface HeaderTopLineProps {
  children: ReactNode
}

const HeaderTopLine: FC<HeaderTopLineProps> = ({ children }) => {
  return <div className={styles.HeaderTopLine}>{children}</div>
}

export default HeaderTopLine
