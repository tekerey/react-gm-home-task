import { renderWithStore, screen } from './utils/test-utils'

import App from './App'

jest.mock('./layout/header/header', () => ({
  __esModule: true,
  default: jest.fn(() => <div>Header</div>),
}))
jest.mock('./views/moviesListView/moviesListView', () => ({
  __esModule: true,
  default: jest.fn(() => <div>MoviesListView</div>),
}))

describe('<App />', () => {
  const testCases = [
    { route: '/' },
    { route: '/search' },
    { route: '/search/matrix' },
    { route: '/search/matrix?genre=Crime&sortBy=title' },
  ]

  it('should render App component', () => {
    const { asFragment } = renderWithStore(<App />)

    expect(asFragment()).toMatchSnapshot()
  })

  it.each(testCases)(
    'should render movie list view for $route route',
    ({ route }) => {
      renderWithStore(<App />, { route })

      expect(screen.getByText('MoviesListView')).toBeInTheDocument()
    }
  )

  it.each(testCases)('should render header for $route route', ({ route }) => {
    renderWithStore(<App />, { route })

    expect(screen.getByText('Header')).toBeInTheDocument()
  })

  it('should render not found error view for unknown route', () => {
    const { asFragment } = renderWithStore(<App />, {
      route: '/some-unknown-route',
    })

    expect(asFragment()).toMatchSnapshot()
    expect(screen.getByText('Page not found')).toBeInTheDocument()

    expect(screen.queryByText('Header')).not.toBeInTheDocument()
    expect(screen.queryByText('MoviesListView')).not.toBeInTheDocument()
  })
})
