export enum Genre {
  DOCUMENTARY = 'Documentary',
  COMEDY = 'Comedy',
  HORROR = 'Horror',
  CRIME = 'Crime',
}

export interface Movie {
  id: number
  genres: Genre[]
  title: string
  tagline?: string
  release_date: string
  poster_path: string
  vote_average: number
  overview: string
  runtime: number
}

export type GenreOrAll = Genre | 'all'

export interface MovieFormData {
  title: string
  releaseDate: string
  movieUrl: string
  rating: string
  genres: Genre[]
  runtime: string
  overview: string
}
