import path from 'path'
import { readFile } from 'fs/promises'
import express from 'express'
import compression from 'compression'
import { renderToString } from 'react-dom/server'
import { StrictMode } from 'react'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router-dom/server'

import { setupStore } from './core'

import App from './web/App'
import ErrorBoundary from './web/components/errorBoundary/errorBoundary'
import { Html } from './web/Html'
import { moviesApi } from './core/movies'

const app = express()
const port = process.env.PORT || 3002

app.use(compression())
app.use('/static', express.static(path.join('dist', 'static')))

app.get('/', (req, res) => {
  res.redirect('/search')
})

app.get('*', async (req, res) => {
  const manifestString = await readFile(
    path.join('dist', 'static', 'manifest.json'),
    'utf-8'
  )
  const manifest = manifestString ? JSON.parse(manifestString) : {}

  const store = setupStore()

  const renderRoot = () => (
    <StrictMode>
      <Provider store={store}>
        <ErrorBoundary>
          <StaticRouter location={req.url}>
            <App />
          </StaticRouter>
        </ErrorBoundary>
      </Provider>
    </StrictMode>
  )

  renderToString(renderRoot())

  await Promise.all(moviesApi.util.getRunningOperationPromises())

  const state = store.getState()

  // a workaround to fix a problem when calling renderToString(renderRoot()) always return UI with loaders
  store.replaceReducer(() => state)

  const content = renderToString(renderRoot())

  res.status(200).send(`
    <!DOCTYPE html>
    ${renderToString(
      <Html
        contentString={content}
        preloadedState={state}
        manifest={manifest}
      />
    )}
  `)
})

app.listen(port, () => console.log(`\nListening on http://localhost:${port}\n`))
