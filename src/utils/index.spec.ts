import {
  createMovieDtoMock,
  movieFormDataMock,
  movieMock,
} from '../mocks/movies'
import { Movie } from '../types/interfaces'
import {
  formDataToCreateMovieDTO,
  formDataToMovie,
  movieToFormData,
} from './index'

describe('formDataToCreateMovieDTO', () => {
  it('should transform form data into create movie DTO', () => {
    expect(formDataToCreateMovieDTO(movieFormDataMock)).toEqual(
      createMovieDtoMock
    )
  })
})

describe('formDataToMovie', () => {
  it('should transform form data into movie data', () => {
    expect(formDataToMovie(movieFormDataMock, 123456)).toEqual(movieMock)
  })
})

describe('movieToFormData', () => {
  it('should transform movie data into form data', () => {
    expect(movieToFormData(movieMock)).toEqual(movieFormDataMock)
  })

  it.each([
    { prop: 'title', formField: 'title' },
    { prop: 'vote_average', formField: 'rating' },
    { prop: 'poster_path', formField: 'movieUrl' },
    { prop: 'overview', formField: 'overview' },
    { prop: 'release_date', formField: 'releaseDate' },
    { prop: 'runtime', formField: 'runtime' },
  ])(
    'should convert non existing $prop movie data into empty string',
    ({ prop, formField }) => {
      const movie: Partial<Movie> = { ...movieMock }
      delete movie[prop]

      const movieFormData = { ...movieFormDataMock }
      movieFormData[formField] = ''

      expect(movieToFormData(movie)).toEqual(movieFormData)
    }
  )

  it('should convert non existing genres movie data into empty array', () => {
    const movie: Partial<Movie> = { ...movieMock }
    delete movie.genres

    const movieFormData = { ...movieFormDataMock }
    movieFormData.genres = []

    expect(movieToFormData(movie)).toEqual(movieFormData)
  })
})
