import * as Yup from 'yup'

export const validation = {
  title: Yup.string().required('Title is a required field'),
  releaseDate: Yup.date().required('Release date is a required field'),
  movieUrl: Yup.string()
    .url('Movie URL must be a valid URL')
    .required('Movie URL is a required field'),
  rating: Yup.number()
    .min(0, 'Rating must be greater than or equal to 0')
    .max(10, 'Rating must be less than or equal to 10'),
  genres: Yup.array(Yup.string()).min(
    1,
    'Select at least one genre to proceed'
  ),
  runtime: Yup.number()
    .integer('Runtime must be an integer')
    .min(0, 'Runtime must be greater than or equal to 0')
    .required('Runtime is a required field'),
  overview: Yup.string().required('Overview is a required field'),
}
