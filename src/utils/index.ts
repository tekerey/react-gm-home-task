import { CreateMovieDTO } from '../core/movies'
import { Movie, MovieFormData } from '../types/interfaces'

export const formDataToCreateMovieDTO = ({
  releaseDate,
  movieUrl,
  rating,
  runtime,
  ...rest
}: MovieFormData): CreateMovieDTO => ({
  poster_path: movieUrl,
  release_date: releaseDate,
  vote_average: +rating,
  runtime: +runtime,
  ...rest,
})

export const formDataToMovie = (
  formData: MovieFormData,
  id: number
): Movie => ({
  ...formDataToCreateMovieDTO(formData),
  id,
})

export const movieToFormData = (movie: Partial<Movie>): MovieFormData => ({
  title: movie?.title || '',
  rating: movie?.vote_average?.toString() || '',
  genres: movie?.genres || [],
  movieUrl: movie?.poster_path || '',
  overview: movie?.overview || '',
  releaseDate: movie?.release_date || '',
  runtime: movie?.runtime?.toString() || '',
})
