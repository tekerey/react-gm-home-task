import { CreateMovieDTO } from '../core/movies'
import { Genre, Movie, MovieFormData } from '../types/interfaces'

export const movieFormDataMock: MovieFormData = {
  genres: [Genre.COMEDY, Genre.CRIME],
  movieUrl: 'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg',
  overview: 'Some text here',
  rating: '7',
  releaseDate: '2017-12-09',
  runtime: '152',
  title: 'Some title',
}

export const createMovieDtoMock: CreateMovieDTO = {
  genres: [Genre.COMEDY, Genre.CRIME],
  title: 'Some title',
  release_date: '2017-12-09',
  poster_path:
    'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg',
  vote_average: 7,
  overview: 'Some text here',
  runtime: 152,
}

export const movieMock: Movie = {
  genres: [Genre.COMEDY, Genre.CRIME],
  title: 'Some title',
  release_date: '2017-12-09',
  poster_path:
    'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII.jpg',
  vote_average: 7,
  overview: 'Some text here',
  runtime: 152,
  id: 123456,
}

export const movieMock2: Movie = {
  genres: [Genre.DOCUMENTARY],
  title: 'Some title 2',
  release_date: '2018-12-09',
  poster_path:
    'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII_2.jpg',
  vote_average: 9,
  overview: 'Some text here 2',
  runtime: 97,
  id: 234567,
}

export const movieMock3: Movie = {
  genres: [Genre.HORROR],
  title: 'Some title 3',
  release_date: '2019-12-09',
  poster_path:
    'https://image.tmdb.org/t/p/w500/kOVEVeg59E0wsnXmF9nrh6OmWII_3.jpg',
  vote_average: 10,
  overview: 'Some text here 3',
  runtime: 121,
  id: 345678,
}

export const moviesMock: Movie[] = [movieMock, movieMock2, movieMock3]
