import { rest } from 'msw'

import { movieMock, moviesMock } from './movies'

export const handlers = [
  rest.post('http://localhost:4000/movies/', (req, res, ctx) => {
    return res(ctx.status(201), ctx.json(movieMock))
  }),
  rest.get('http://localhost:4000/movies/123456', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(movieMock))
  }),
  rest.get('http://localhost:4000/movies/', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json({ data: moviesMock }))
  }),
]
