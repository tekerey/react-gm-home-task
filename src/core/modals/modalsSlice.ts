import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { Movie } from '../../types/interfaces'
import { Modal } from '../../web/components/modals/modals'
import { RootState } from '../store'

export interface ModalsState {
  modal: Modal | null
  movieForModal: Movie | null
}

const initialState: ModalsState = {
  modal: null,
  movieForModal: null,
}

export const modalsSlice = createSlice({
  name: 'modals',
  initialState,
  reducers: {
    openModal: (
      state,
      action: PayloadAction<{ modal: Modal; movieForModal?: Movie }>
    ) => {
      state.modal = action.payload.modal
      state.movieForModal = action.payload.movieForModal ?? null
    },
    closeModal: state => {
      state.modal = null
      state.movieForModal = null
    },
  },
})

export const reducer = modalsSlice.reducer
export const modalActions = modalsSlice.actions

export const selectModal = (state: RootState) => state.modals.modal
export const selectMovieForModal = (state: RootState) =>
  state.modals.movieForModal
