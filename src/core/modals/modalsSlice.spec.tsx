import { movieMock } from '../../mocks/movies'
import { Modal } from '../../web/components/modals/modals'
import { modalActions, ModalsState, reducer } from './modalsSlice'

describe('modalsSlice', () => {
  describe('reducer', () => {
    it('should return the initial state', () => {
      expect(reducer(undefined, { type: undefined })).toEqual({
        modal: null,
        movieForModal: null,
      } as ModalsState)
    })

    it('should handle opening modal without movie for modal', () => {
      expect(
        reducer(undefined, modalActions.openModal({ modal: Modal.ADD_MOVIE }))
      ).toEqual({
        modal: Modal.ADD_MOVIE,
        movieForModal: null,
      } as ModalsState)
    })

    it('should handle opening modal with movie for modal', () => {
      expect(
        reducer(
          undefined,
          modalActions.openModal({
            modal: Modal.EDIT_MOVIE,
            movieForModal: movieMock,
          })
        )
      ).toEqual({
        modal: Modal.EDIT_MOVIE,
        movieForModal: movieMock,
      } as ModalsState)
    })

    it('should handle closing modal', () => {
      expect(reducer(undefined, modalActions.closeModal())).toEqual({
        modal: null,
        movieForModal: null,
      } as ModalsState)
    })
  })
})
