import {
  combineReducers,
  configureStore,
  PreloadedState,
} from '@reduxjs/toolkit'

import { moviesApi } from './movies'
import { reducer as modalsReducer } from './modals'

const rootReducer = combineReducers({
  modals: modalsReducer,
  [moviesApi.reducerPath]: moviesApi.reducer,
})

export const setupStore = (preloadedState?: PreloadedState<RootState>) => {
  return configureStore({
    reducer: rootReducer,
    middleware: getDefaultMiddleware =>
      getDefaultMiddleware().concat(moviesApi.middleware),
    preloadedState,
  })
}

export type RootState = ReturnType<typeof rootReducer>
