import 'cross-fetch/polyfill'
import { fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { Genre, Movie } from '../../types/interfaces'

import { createApi } from '../utils'

interface GetMoviesResponse {
  data: Movie[]
}

interface GetMoviesOptions {
  search?: string
  filterBy?: Genre | null
  sortBy?: string | null
}

export type CreateMovieDTO = Omit<Movie, 'id'>

export const moviesApi = createApi({
  reducerPath: 'moviesList',
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:4000/movies/' }),
  tagTypes: ['Movies', 'Movie'],
  endpoints: build => ({
    getMovies: build.query<Movie[], GetMoviesOptions | void>({
      query: ({ search, filterBy, sortBy } = {}) => ({
        url: '/',
        params: {
          sortBy: sortBy || 'release_date',
          sortOrder: sortBy === 'title' ? 'asc' : 'desc',
          ...(search && {
            search,
            searchBy: 'title',
          }),
          ...(filterBy && { filter: [filterBy] }),
        },
      }),
      transformResponse: (response: GetMoviesResponse) => response.data,
      providesTags: ['Movies'],
    }),
    getMovie: build.query<Movie, string>({
      query: id => ({
        url: `/${id}`,
      }),
      providesTags: ['Movie'],
    }),
    addMovie: build.mutation<Movie, CreateMovieDTO>({
      query: newMovie => ({
        url: '/',
        method: 'POST',
        body: newMovie,
      }),
      invalidatesTags: ['Movies'],
    }),
    updateMovie: build.mutation<Movie, Movie>({
      query: movie => ({
        url: '/',
        method: 'PUT',
        body: movie,
      }),
      invalidatesTags: ['Movies', 'Movie'],
    }),
    deleteMovie: build.mutation<void, number>({
      query: id => ({
        url: `/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['Movies'],
    }),
  }),
})

export const {
  useGetMoviesQuery,
  useGetMovieQuery,
  useAddMovieMutation,
  useUpdateMovieMutation,
  useDeleteMovieMutation,
} = moviesApi
