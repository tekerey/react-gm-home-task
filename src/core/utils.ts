import {
  buildCreateApi,
  coreModule,
  createApi as createClientApi,
  reactHooksModule,
} from '@reduxjs/toolkit/query/react'

declare const __IS_SERVER__: boolean

const createSSRApi = buildCreateApi(
  coreModule(),
  reactHooksModule({ unstable__sideEffectsInRender: true })
)

export const createApi = __IS_SERVER__ ? createSSRApi : createClientApi
