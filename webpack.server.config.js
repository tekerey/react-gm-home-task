const path = require('path')

const webpack = require('webpack')
const webpackNodeExternals = require('webpack-node-externals')

module.exports = env => {
  const isDev = env.development

  return {
    entry: './src/server.tsx',
    target: 'node',
    mode: isDev ? 'development' : 'production',
    ...(isDev && { devtool: 'source-map' }),
    output: {
      filename: 'server.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: '/',
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    module: {
      rules: [
        {
          test: /\.(js|ts)x?$/,
          exclude: /node_modules/,
          use: ['babel-loader'],
        },
        {
          test: /\.css$/i,
          exclude: /node_modules/,
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: {
                  exportOnlyLocals: true,
                },
              },
            },
          ],
        },
      ],
    },
    plugins: [
      new webpack.DefinePlugin({
        __IS_SERVER__: true,
      }),
    ],
    externals: webpackNodeExternals(),
  }
}
