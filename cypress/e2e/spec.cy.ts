describe('search', () => {
  it('should search for movie and display its info when clicked', () => {
    cy.visit('http://localhost:3000/')
    cy.get('input[type="search"]').type('star wars {enter}')

    cy.url().should('equal', 'http://localhost:3000/search/star%20wars')

    cy.contains('Star Wars: The Last Jedi').click()

    cy.url().should(
      'equal',
      'http://localhost:3000/search/star%20wars?movie=181808'
    )
    cy.get('[data-test-id="search-bar"]').should('not.exist')

    cy.get('[data-test-id="movie-info"]').should('be.visible')
    cy.get('[data-test-id="movie-info-title"]')
      .should('be.visible')
      .and('have.text', 'Star Wars: The Last Jedi')
  })
})
