const path = require('path')

const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin')
const { WebpackManifestPlugin } = require('webpack-manifest-plugin')

module.exports = env => {
  const isDev = env.development
  const isSSR = env.ssr

  return {
    entry: isSSR ? './src/web.tsx' : './src/index.tsx',
    mode: isDev ? 'development' : 'production',
    ...(isDev && { devtool: 'source-map' }),
    output: {
      filename: isDev ? 'bundle.js' : '[name].[contenthash].js',
      path: isSSR
        ? path.resolve(__dirname, 'dist', 'static')
        : path.resolve(__dirname, 'dist'),
      publicPath: '/',
    },
    resolve: {
      extensions: ['.tsx', '.ts', '.js'],
    },
    module: {
      rules: [
        {
          test: /\.(js|ts)x?$/,
          exclude: /node_modules/,
          use: ['babel-loader'],
        },
        {
          test: /\.css$/i,
          exclude: /node_modules/,
          use: [
            isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
            'css-loader',
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: ['postcss-preset-env'],
                },
              },
            },
          ],
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          type: 'asset/resource',
          generator: {
            publicPath: isSSR ? '/static/assets/' : '/assets/',
            outputPath: 'assets/',
          },
        },
      ],
    },
    devServer: {
      historyApiFallback: true,
    },
    plugins: [
      new webpack.DefinePlugin({
        __IS_SERVER__: false,
      }),
      new WebpackManifestPlugin(),
      ...(!isSSR
        ? [
            new HtmlWebpackPlugin({
              template: path.resolve(__dirname, 'public', 'index.html'),
            }),
          ]
        : []),
      ...(!isDev
        ? [
            new MiniCssExtractPlugin({
              filename: '[name].[contenthash].css',
            }),
          ]
        : []),
    ],
    ...(!isDev && {
      optimization: {
        minimizer: [`...`, new CssMinimizerPlugin()],
        moduleIds: 'deterministic',
        runtimeChunk: 'single',
        splitChunks: {
          cacheGroups: {
            vendor: {
              test: /[\\/]node_modules[\\/]/,
              name: 'vendors',
              chunks: 'all',
            },
            styles: {
              name: 'styles',
              type: 'css/mini-extract',
              chunks: 'all',
              enforce: true,
            },
          },
        },
      },
    }),
  }
}
